
let result = [];

function filter(elements, callback){
    for (let index=0;index<elements.length; index++){
        let isExist = callback(elements[index],index,elements);
        if (isExist === true){
            result.push(elements[index])
        }
    }
    return result
}

module.exports = filter;

