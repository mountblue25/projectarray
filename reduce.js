
function reduce(elements, callback, startingValue) {
    if (startingValue === undefined){
        startingValue = elements[0]
        for (let index=1; index< elements.length; index++){
            startingValue = callback(startingValue,elements[index],index, elements)
        }
        return startingValue
    } else {
        for (let index=0; index<elements.length; index++){
            startingValue = callback(startingValue, elements[index],index, elements);
        }
        return startingValue
  }
}

module.exports = reduce;

