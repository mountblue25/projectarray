
function find(elements, callback){
    for (let index=0;index<elements.length; index++){
        let result = callback(elements[index]);
        if (result === true) {
            return elements[index];
        }
    }
}

module.exports = find;